function tambah(a, b) {
  let result = 0;

  // .. tulis jawaban kamu disini
  result += a + b;
  return parseFloat(result);
}

function kurang(a, b) {
  let result = 0;

  // .. tulis jawaban kamu disini
  result += a - b;
  return parseFloat(result);
}

function bagi(a, b) {
  let result = 0;

  // .. tulis jawaban kamu disini
  result += a / b;
  return parseFloat(result);
}

function kali(a, b) {
  let result = 0;

  // .. tulis jawaban kamu disini
  result += a * b;
  return parseFloat(result);
}

if (typeof window == "undefined") {
  module.exports = { tambah, kurang, bagi, kali };
}
